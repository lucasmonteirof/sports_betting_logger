class Bet < ApplicationRecord
  belongs_to :user
  validates_presence_of :date, :sport, :location, :competition, :event,
    :market, :bet_choice, :odds, :underdog, :suggested, :influencer,
    :bet_size, :value, :return_value, :user_id
end
