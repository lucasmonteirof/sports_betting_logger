class User < ApplicationRecord
  has_many :bets
  validates_uniqueness_of :name
  validates_presence_of :name, :profit
end
