require 'rails_helper'

RSpec.describe Bet, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of :date }
    it { is_expected.to validate_presence_of :sport }
    it { is_expected.to validate_presence_of :location }
    it { is_expected.to validate_presence_of :competition }
    it { is_expected.to validate_presence_of :event }
    it { is_expected.to validate_presence_of :market }
    it { is_expected.to validate_presence_of :bet_choice }
    it { is_expected.to validate_presence_of :odds }
    it { is_expected.to validate_presence_of :underdog }
    it { is_expected.to validate_presence_of :suggested }
    it { is_expected.to validate_presence_of :bet_size }
    it { is_expected.to validate_presence_of :value }
    it { is_expected.to validate_presence_of :return_value }
    it { is_expected.to validate_presence_of :user_id }

    it { is_expected.to belong_to :user }
  end
end
