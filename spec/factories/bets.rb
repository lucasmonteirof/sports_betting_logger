FactoryGirl.define do
  factory :bet do
    date "2017-10-23"
    sport "MyString"
    location "MyString"
    competition "MyString"
    event "MyString"
    market "MyString"
    bet_choice "MyString"
    odds "9.99"
    underdog false
    suggested false
    influencer "MyString"
    bet_size "MyString"
    value "9.99"
    return_value "9.99"
    user nil
  end
end
