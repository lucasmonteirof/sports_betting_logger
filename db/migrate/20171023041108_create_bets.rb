class CreateBets < ActiveRecord::Migration[5.1]
  def change
    create_table :bets do |t|
      t.date :date
      t.string :sport
      t.string :location
      t.string :competition
      t.string :event
      t.string :market
      t.string :bet_choice
      t.decimal :odds
      t.boolean :underdog
      t.boolean :suggested
      t.string :influencer
      t.string :bet_size
      t.decimal :value
      t.decimal :return_value
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
